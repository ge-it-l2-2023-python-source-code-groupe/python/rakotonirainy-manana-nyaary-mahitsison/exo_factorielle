from factoriell import calculate_factorial

def main():
    n = int(input("Entrez un nombre entier pour calculer sa factorielle : "))
    if n < 0:
        print("La factorielle n'est définie que pour les entiers positifs.")
    else:
        result = calculate_factorial(n)
        print(f"{n}! = {result}")

if __name__ == "__main__":
    main()
